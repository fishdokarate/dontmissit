import React, { useContext, useEffect, useRef, useState } from 'react';
import { defaultSelectors, defaultServices } from './initial';
import { StateContext } from './data/StateProvider';
import Modal from './components/Modal';
import ServiceBlock from './components/ServiceBlock';
import ServiceButton from './components/ServiceButton';
import ServiceForm from './components/ServiceForm';
import SettingsForm from './components/SettingsForm';
import bellImg from './assets/bell.png';
import settingsImg from './assets/settings.png';
import comeBackAliveImg from './assets/come_back_alive.png';
import moment from 'moment-timezone';
import { nextUpdateTime, nextUpdateInterval, filterCanIUpdateInTime } from './utils/date';
import { useIndexDb } from './utils/useIndexDb';

export const AppContext = React.createContext();
const ws = new WebSocket('ws://localhost:8000/api/ws');
const datetime = moment.tz.setDefault('Etc/UTC');

const App = () => {
  const { state, dispatch } = useContext(StateContext);
  const [, useStore] = useIndexDb('dontmissit', 1, 'services', defaultServices);
  const [, store, setStore] = useStore('services');
  const [isSettingsOpened, setSettingsOpened] = useState(false);
  const stateServices = Object.keys(state.services);
  const serviceRefs = stateServices.reduce((acc, val) => ({ ...acc, [val]: useRef() }), {});
  const newServiceRef = useRef(null);
  const isServicesAvailable =
    Object.values(state.services).filter((service) => Object.keys(service).length > 0).length > 0;
  const isServicePagesAvailable = (serviceName) => {
    return Object.keys(state.services[serviceName]).length > 0;
  };
  const isServiceTwitsAvailable = (serviceName) => {
    return (
      Object.values(state.services[serviceName]).filter((page) => page.twits.length > 0).length > 0
    );
  };

  const handleUpdate = (isFirstTime) => {
    const serviceAndPageList = Object.entries(state.services).reduce(
      (arrServsAndPages, [serviceName, pageList]) => {
        const pgs = Object.entries(pageList).reduce((arrPages, [pageName, page]) => {
          if (page.isFilterOn && (isFirstTime || filterCanIUpdateInTime(page.nextUpdateTime))) {
            return [...arrPages, { serviceName, pageName: page.url, keywords: page.keywords }];
          }
          return arrPages;
        }, []);
        return [...arrServsAndPages, ...pgs];
      },
      []
    );
    if (serviceAndPageList.length) {
      console.log(`оновлення для (${JSON.stringify(serviceAndPageList)})`);
      ws.send(
        JSON.stringify({
          type: 'FETCH_DATA',
          payload: {
            data: serviceAndPageList,
            selectors: defaultSelectors,
            period: state.periodUpdateTime
          }
        })
      );
      dispatch({
        type: 'HANDLE_GLOBAL_TIMER',
        payload: nextUpdateTime(state.periodUpdateTime)
      });
    }
  };

  useEffect(() => {
    if (store[0]) {
      dispatch({ type: 'SET_ALL_SERVICES', payload: store[0] });
      handleUpdate(true);
    }
  }, [store]);

  useEffect(() => {
    const flags = {
      isServicesChanged: state.isServicesChanged,
      isPagesChanged: state.isPagesChanged,
      isTwitsChanged: state.isTwitsChanged
    };
    const flagsKeys = Object.entries(flags).filter(([key, val]) => val);
    if (flagsKeys.length) {
      setStore(state.services);
      for (const [flagKey] of flagsKeys) {
        dispatch({
          type: 'SWITCH_FLAG',
          payload: {
            flagKey
          }
        });
      }
    }
  }, [state.isServicesChanged, state.isPagesChanged, state.isTwitsChanged]);

  useEffect(() => {
    clearInterval(state.periodUpdateTimeId);
    const newPeriodId = setInterval(handleUpdate, nextUpdateInterval(state.periodUpdateTime));
    dispatch({
      type: 'SET_PERIOD_ID',
      payload: newPeriodId
    });
  }, [state.periodUpdateTime]);

  useEffect(() => {
    ws.addEventListener('message', (e) => {
      try {
        const { err, data } = JSON.parse(e.data);
        if (err) throw err;
        console.log(`клієнт отримав (${JSON.stringify(data)})`);
        for (const serviceData of data) {
          const { serviceName, pageName, twits } = serviceData;
          if (twits.length < 1)
            console.warn(`для ${JSON.stringify(serviceData)} немає актуальних даних`);
          dispatch({
            type: 'ADD_TWITS',
            payload: { serviceName, pageName, twits }
          });
        }
      } catch (err) {
        const errText = err + '';
        console.error(errText);
        dispatch({ type: 'SHOW_MODAL_CONTENT', payload: errText });
      }
    });
    const periodId = setInterval(handleUpdate, nextUpdateInterval(state.periodUpdateTime));
    dispatch({
      type: 'SET_PERIOD_ID',
      payload: periodId
    });
  }, []);

  return (
    <AppContext.Provider value={{ ws, serviceRefs }}>
      <div className="app center-row">
        <header className="container header-container">
          <div className="app-header-block">
            <a className="app-logo">
              <h1 className="app-logo__title">D!</h1>
              <img className="app-logo__img" src={bellImg} />
            </a>
            <ul className="app-srv__list center-col">
              {stateServices.length > 0 &&
                stateServices.map(
                  (service) =>
                    service && (
                      <li className="app-srv__list__item" key={service}>
                        <ServiceButton serviceName={service} isDissabled={!isServicesAvailable} />
                      </li>
                    )
                )}
              <li className="app-srv__list__item">
                <button
                  className="app-srv__list__add-button"
                  onClick={() => {
                    newServiceRef.current.scrollIntoView({
                      behavior: 'smooth',
                      block: 'center'
                    });
                    setSettingsOpened(false);
                  }}
                >
                  +
                </button>
              </li>
            </ul>
          </div>
          <div className="app-header-block">
            <ul className="app-srv__list center-col">
              <li className="app-srv__list__item">
                <button
                  className="app-srv__list__add-button "
                  style={{
                    backgroundImage: `url("${settingsImg}")`
                  }}
                  onClick={() => {
                    newServiceRef.current.scrollIntoView({
                      behavior: 'smooth',
                      block: 'center'
                    });
                    setSettingsOpened(true);
                  }}
                ></button>
              </li>
            </ul>
          </div>
        </header>
        <div className="container full-container">
          {state.isModalOpened && <Modal />}
          <section className={'app-srv-block ' + (!isServicesAvailable && 'full-block')}>
            {stateServices.length > 0 &&
              stateServices.map(
                (service) =>
                  service &&
                  isServicePagesAvailable(service) && (
                    <ServiceBlock key={service} serviceName={service} />
                  )
              )}
            <section className="app-srv-block__list center-col" ref={newServiceRef}>
              {isSettingsOpened ? <SettingsForm /> : <ServiceForm ws={ws} />}
              <div className="app-srv-block__list__add-donate">
                <a
                  className="app-srv__list center-col"
                  href="https://savelife.in.ua"
                  title="Домашня сторінка фонду Повернись живим"
                  target="_blank"
                  rel="noreferrer"
                >
                  <h3>Підтримуємо Збройні Сили України!</h3>
                  <img src={comeBackAliveImg} alt="Домашня сторінка фонду Повернись живим" />
                </a>
              </div>
            </section>
          </section>
        </div>
      </div>
    </AppContext.Provider>
  );
};

export default App;
