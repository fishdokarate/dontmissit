export const defaultServices = {
  // facebook: {},
  twitter: {},
  telegram: {}
};

export const defaultSelectors = {
  // facebook: {
  //   url: 'facebook.com',
  //   loginUrl: 'login',
  //   loginForm: 'form[id="login_form"]',
  //   loginButton: 'button[id="loginbutton"]',
  //   username: 'input[id="email"]',
  //   password: 'input[id="pass"]',
  //   scrollDirection: 'bottom',
  //   main: 'div[role="main"]',
  //   twit: 'div[role="article"]',
  //   desc: 'div[role="article"] div[data-ad-preview="message"]',
  //   img: 'div[role="article"] div>img[src], div[role="article"] div>video[src]',
  //   link: 'div[role="article"] span:nth-child(2)>span>a:has(> span)[href]',
  //   time: 'div[role="article"] span:nth-child(2)>span>a>span',
  //   closeModal: 'div[aria-label="Close"]'
  // },
  twitter: {
    url: 'twitter.com',
    loginUrl: 'login',
    loginForm: 'div[aria-labelledby="modal-header"]',
    loginButton: 'div[data-testid="LoginForm_Login_Button"]',
    nextButton: 'div[data-viewportview="true"]>div>div>div:nth-child(6)',
    phoneOrUsername: 'input[data-testid="ocfEnterTextTextInput"]',
    phoneOrUsernameButton: 'div[data-testid="ocfEnterTextNextButton"]',
    username: 'input[autocomplete="username"]',
    password: 'input[autocomplete="current-password"]',
    scrollDirection: 'bottom',
    main: 'div[data-testid="primaryColumn"]',
    twit: 'article[role="article"]',
    desc: 'article[role="article"] div[data-testid="tweetText"]',
    img: 'article[role="article"] div[data-testid="tweetPhoto"] img[src], article[role="article"] div[data-testid="videoPlayer"] video[poster]',
    link: 'article[role="article"] a:has(> time)[href]',
    time: 'article[role="article"] time[datetime]',
    closeModal: 'div[aria-label="Close"]'
  },
  telegram: {
    url: 't.me',
    previewForm: '.tgme_page',
    previewButton: '.tgme_page_context_link',
    scrollDirection: 'top',
    main: '.tgme_main',
    twit: '.tgme_widget_message',
    desc: '.tgme_widget_message_text',
    img: '.tgme_widget_message_link_preview[href], .tgme_widget_message_photo_wrap[style*="background-image"], .tgme_widget_message_video_thumb[style*="background-image"]',
    link: '.tgme_widget_message_date:has(> time)[href]',
    time: '.tgme_widget_message_date time[datetime]'
  }
};

export const defaultPeriod = '50m';

export const defaultDateFormat = 'lll';
