export const reducer = (state, action) => {
  switch (action.type) {
    case 'ADD_PAGE': {
      const { serviceName, newPage } = action.payload;
      state.services[serviceName] = {
        ...state.services[serviceName],
        [newPage.url]: newPage
      };
      return {
        ...state,
        isModalOpened: true,
        modalContent: 'додано новий елемент',
        isPagesChanged: true
      };
    }
    case 'FILTER_PAGE': {
      const { serviceName, pageName, isFilterOn } = action.payload;
      state.services[serviceName][pageName] = {
        ...state.services[serviceName][pageName],
        isFilterOn
      };
      return {
        ...state,
        isModalOpened: true,
        modalContent: 'додано новий фільтр'
      };
    }
    case 'REMOVE_PAGE': {
      const { serviceName, pageName } = action.payload;
      state.services[serviceName] = Object.fromEntries(
        Object.entries(state.services[serviceName]).filter(([name]) => name !== pageName)
      );
      return {
        ...state,
        isModalOpened: true,
        modalContent: 'видалено елемент',
        isPagesChanged: true
      };
    }
    case 'SHOW_MODAL_CONTENT': {
      return {
        ...state,
        isModalOpened: true,
        modalContent: action.payload
      };
    }
    case 'CLOSE_MODAL': {
      return {
        ...state,
        isModalOpened: false
      };
    }
    case 'HANDLE_GLOBAL_TIMER': {
      return {
        ...state,
        nextUpdateTime: action.payload
      };
    }
    case 'HANDLE_PAGE_TIMER': {
      const { serviceName, pageName, nextUpdateTime } = action.payload;
      state.services[serviceName][pageName] = {
        ...state.services[serviceName][pageName],
        nextUpdateTime
      };
      return {
        ...state
      };
    }
    case 'HANDLE_PERIOD_TIME': {
      return {
        ...state,
        periodUpdateTime: action.payload
      };
    }
    case 'SET_PERIOD_ID': {
      return {
        ...state,
        periodUpdateTimeId: action.payload
      };
    }
    case 'ADD_TWITS': {
      const { serviceName, pageName, twits } = action.payload;
      state.services[serviceName][pageName].twits = [
        ...new Map(
          [...state.services[serviceName][pageName].twits, ...twits].map((item) => [
            item.link,
            item
          ])
        ).values()
      ];
      return {
        ...state,
        isModalOpened: true,
        modalContent: 'оновлення даних',
        isTwitsChanged: true
      };
    }
    case 'REMOVE_ALL_TWITS': {
      const { serviceName } = action.payload;
      state.services[serviceName] = Object.fromEntries(
        Object.entries(state.services[serviceName]).map(([pageName, page]) => [
          pageName,
          {
            ...page,
            twits: []
          }
        ])
      );
      return {
        ...state,
        isModalOpened: true,
        modalContent: 'видалено усі елементи',
        isTwitsChanged: true
      };
    }
    case 'REMOVE_TWIT': {
      const { serviceName, pageName, twitId } = action.payload;
      state.services[serviceName][pageName].twits = state.services[serviceName][
        pageName
      ].twits.filter((twit) => twit.id !== twitId);
      return {
        ...state,
        isModalOpened: true,
        modalContent: 'видалено елемент',
        isTwitsChanged: true
      };
    }
    case 'SELECT_SERVICE': {
      const { serviceName } = action.payload;
      return {
        ...state,
        selectedService: serviceName
      };
    }
    case 'SET_ALL_SERVICES': {
      state.services = {
        ...action.payload
      };
      return {
        ...state
      };
    }
    case 'SWITCH_FLAG': {
      const { flagKey } = action.payload;
      state[flagKey] = !state[flagKey];
      return {
        ...state
      };
    }
    default:
      throw new Error('немає жодної дії');
  }
};
