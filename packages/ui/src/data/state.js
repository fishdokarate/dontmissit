import { defaultServices, defaultPeriod } from '../initial';

export const state = {
  isModalOpened: false,
  modalContent: '',
  isServicesChanged: false,
  isPagesChanged: false,
  isTwitsChanged: false,
  nextUpdateTime: '',
  periodUpdateTime: defaultPeriod,
  periodUpdateTimeId: null,
  services: defaultServices,
  selectedService: ''
};
