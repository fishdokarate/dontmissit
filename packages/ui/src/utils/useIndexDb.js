import { useEffect, useState } from 'react';
import connectToDb from './idxdb';

export const useIndexDb = (dbName, dbVersion, storeName, defaultData) => {
  const [error, setError] = useState(null);
  const { initDb, getStore, updateStore } = connectToDb(dbName, dbVersion);

  const init = async () => {
    try {
      return await initDb(storeName, defaultData);
    } catch (err) {
      setError(err);
    }
  };

  useEffect(() => {
    init();
  }, [dbName, dbVersion]);

  return [
    error,
    (storeName) => {
      const [error, setError] = useState(null);
      const [store, setStore] = useState([]);

      const getData = async () => {
        try {
          const data = await getStore(storeName);
          setStore(data);
        } catch (err) {
          setError(err);
        }
      };
      const updateData = async (data) => {
        try {
          const newData = await updateStore(storeName, 1, data);
          setStore(newData);
        } catch (err) {
          setError(err);
        }
      };

      useEffect(() => {
        getData();
      }, [storeName]);

      return [error, store, updateData];
    }
  ];
};
