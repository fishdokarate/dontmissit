import { useEffect, useState } from 'react';

export const useImage = (fileName) => {
  const [error, setError] = useState(null);
  const [image, setImage] = useState(null);

  const fetchImage = async () => {
    try {
      const response = await import(`../assets/${fileName}.png`);
      setImage(response.default);
    } catch (err) {
      setError(err);
    }
  };

  useEffect(() => {
    fetchImage();
  }, [fileName]);

  return {
    error,
    image
  };
};
