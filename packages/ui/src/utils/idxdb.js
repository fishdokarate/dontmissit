export default (dbName, dbVersion) => {
  const workWithDb = async (fn) => {
    const connectToDb = () => new Promise((resolve) => resolve(indexedDB.open(dbName, dbVersion)));
    const req = await connectToDb();
    return await fn(req);
  };

  const initDb = async (storeName, data) => {
    return await workWithDb((req) => {
      return new Promise((resolve) => {
        req.onupgradeneeded = () => {
          const db = req.result;
          if (!db.objectStoreNames.contains(storeName)) {
            const store = db.createObjectStore(storeName, { autoIncrement: true });
            store.add(data);
            resolve(data);
          }
        };
        req.onerror = () => {
          resolve(false);
        };
      });
    });
  };

  const getStore = async (storeName) => {
    return await workWithDb((req) => {
      return new Promise((resolve, reject) => {
        req.onsuccess = () => {
          const db = req.result;
          const tx = db.transaction(storeName, 'readonly');
          const store = tx.objectStore(storeName);
          const res = store.getAll();
          res.onsuccess = () => {
            resolve(res.result);
          };
          res.onerror = () => {
            reject(req.error);
          };
        };
      });
    });
  };

  const addStore = async (storeName, data) => {
    return await workWithDb((req) => {
      return new Promise((resolve, reject) => {
        req.onsuccess = () => {
          const db = req.result;
          const tx = db.transaction(storeName, 'readwrite');
          const store = tx.objectStore(storeName);
          store.add(data);
          return data;
        };
        req.onerror = () => {
          reject(req.error);
        };
      });
    });
  };

  const updateStore = async (storeName, key, data) => {
    return await workWithDb((req) => {
      return new Promise((resolve, reject) => {
        req.onsuccess = () => {
          const db = req.result;
          const tx = db.transaction(storeName, 'readwrite');
          const store = tx.objectStore(storeName);
          const res = store.get(key);
          res.onsuccess = () => {
            const newData = { ...res.result, ...data };
            store.put(newData, key);
            resolve(newData);
          };
          req.onerror = () => {
            reject(req.error);
          };
        };
      });
    });
  };

  const deleteStore = async (storeName, key) => {
    return await workWithDb((req) => {
      return new Promise((resolve, reject) => {
        req.onsuccess = () => {
          const db = req.result;
          const tx = db.transaction(storeName, 'readwrite');
          const store = tx.objectStore(storeName);
          const res = store.delete(key);
          res.onsuccess = () => {
            resolve(true);
          };
          res.onerror = () => {
            reject(req.error);
          };
        };
      });
    });
  };

  return { initDb, getStore, addStore, updateStore, deleteStore };
};
