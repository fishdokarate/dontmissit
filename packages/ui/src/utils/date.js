import moment from 'moment-timezone';
import 'moment/locale/uk';

import { defaultDateFormat } from '../initial.js';

const datetime = moment.tz.setDefault('Etc/UTC');

const sliceDate = (date) => {
  const result = date.match(/(\d+)([а-яa-z]$)/);
  return { num: result ? result[1] : '', token: result ? result[2] : '' };
};

const addDate = (num, token) => {
  return datetime().add(num, token);
};

const formatDate = (time) => {
  if (!datetime(time).isValid()) {
    const { num, token } = sliceDate(time);
    return +addDate(dateNum, dateToken);
  }
  return +datetime(time);
};

const formatLocalDate = (time) => {
  return moment.tz(time, 'Europe/Kiev').locale('uk').format(defaultDateFormat);
};

const nextUpdateTime = (periodTime) => {
  const { num: periodNum, token: periodToken } = sliceDate(periodTime);
  const dateOfUpdate = addDate(periodNum, periodToken);
  return formatDate(dateOfUpdate);
};

const nextUpdateInterval = (periodTime) => {
  const { num: periodNum, token: periodToken } = sliceDate(periodTime);
  return +periodNum * 60 * 1000;
};

const sortingTwits = (twits, order) => {
  return twits.sort((a, b) => {
    const [c, d] = order === 'desc' ? [b, a] : [a, b];
    return datetime(c.date) - datetime(d.date);
  });
};

const filterCanIUpdateInTime = (time) => {
  const nextUpdateTime = formatDate(time);
  const diffTime = datetime().diff(nextUpdateTime, 's');
  return +diffTime > 0;
};

export {
  formatLocalDate,
  nextUpdateTime,
  nextUpdateInterval,
  sortingTwits,
  filterCanIUpdateInTime
};
