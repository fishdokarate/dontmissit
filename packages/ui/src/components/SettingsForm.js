import React, { useContext, useState } from 'react';
import { StateContext } from '../data/StateProvider';

const SettingsForm = () => {
  const { state, dispatch } = useContext(StateContext);
  const [period, setPeriod] = useState(state.periodUpdateTime);

  const handleSettingsSubmit = (e) => {
    e.preventDefault();
    if (period) {
      dispatch({
        type: 'HANDLE_PERIOD_TIME',
        payload: period
      });
    }
  };

  return (
    <form className="app-srv-block__list__add-form" onSubmit={handleSettingsSubmit}>
      <h2>Налаштування</h2>
      <label className="app-srv-block__list__add-form_input">
        <span>Період</span>
        <input type="text" value={period} onChange={(e) => setPeriod(e.target.value)} />
      </label>
      <button className="app-srv-block__list__dropdown_item__add-button" type="submit">
        Зберегти
      </button>
    </form>
  );
};

export default SettingsForm;
