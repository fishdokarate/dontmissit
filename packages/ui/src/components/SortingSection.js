import React, { useState, useContext } from 'react';
import { StateContext } from '../data/StateProvider';

const SortingSection = ({ order, onSort }) => {
  const { state, dispatch } = useContext(StateContext);

  return (
    <ul className="app-srv-block__list__dropdown_block">
      <li className="app-srv-block__list__dropdown_item">
        <span>Дата</span>
        <span>
          <button
            className={
              'app-srv-block__list__dropdown_item__add-button ' +
              (order === 'desc' && 'filter--active')
            }
            onClick={() => onSort('desc')}
          >
            нові
          </button>
          <button
            className={
              'app-srv-block__list__dropdown_item__add-button ' +
              (order !== 'desc' && 'filter--active')
            }
            onClick={() => onSort('asc')}
          >
            старі
          </button>
        </span>
      </li>
    </ul>
  );
};

export default SortingSection;
