import React, { useContext, useState } from 'react';
import { StateContext } from '../data/StateProvider';
import { defaultSelectors } from '../initial';
import { nextUpdateTime } from '../utils/date';
import ServiceButton from './ServiceButton';

const ServiceForm = ({ ws }) => {
  const { state, dispatch } = useContext(StateContext);
  const [url, setUrl] = useState('');
  const [keys, setKeys] = useState('');
  const stateServices = Object.keys(state.services);

  const handleServiceSubmit = (e) => {
    e.preventDefault();

    const serviceName = state.selectedService;
    const pageName = url;
    const keywords = keys;
    if (serviceName && pageName) {
      const newPage = {
        id: new Date().getTime().toString(),
        url,
        keywords,
        twits: [],
        isFilterOn: true,
        nextUpdateTime: nextUpdateTime(state.periodUpdateTime)
      };
      dispatch({ type: 'ADD_PAGE', payload: { serviceName, newPage } });
      setUrl('');
      setKeys('');

      console.log(`оновлення для (${JSON.stringify(newPage)})`);
      ws.send(
        JSON.stringify({
          type: 'FETCH_DATA',
          payload: {
            data: [{ serviceName, pageName, keywords }],
            selectors: defaultSelectors,
            period: state.periodUpdateTime
          }
        })
      );
      dispatch({
        type: 'HANDLE_PAGE_TIMER',
        payload: {
          serviceName,
          pageName,
          nextUpdateTime: nextUpdateTime(state.periodUpdateTime)
        }
      });
      if (!state.nextUpdateTime) {
        dispatch({
          type: 'HANDLE_GLOBAL_TIMER',
          payload: nextUpdateTime(state.periodUpdateTime)
        });
      }
    } else {
      const errText = 'будь ласка введіть дані';
      dispatch({ type: 'SHOW_MODAL_CONTENT', payload: errText });
    }
  };

  return (
    <form className="app-srv-block__list__add-form" onSubmit={handleServiceSubmit}>
      <h2>
        Додай сторінку <br /> для відслідковування
      </h2>
      <ul className="app-srv__list center-row">
        {stateServices.length > 0 &&
          stateServices.map(
            (service) =>
              service && (
                <li className="app-srv__list__item" key={service}>
                  <ServiceButton serviceName={service} isFromAddForm={true} />
                </li>
              )
          )}
      </ul>
      {state.selectedService && (
        <div className="app-srv-block__list__add-form_block center-col">
          <label className="app-srv-block__list__add-form_input">
            <span>Сайт</span>
            <input type="text" value={url} onChange={(e) => setUrl(e.target.value)} />
          </label>
          <label className="app-srv-block__list__add-form_input">
            <span>Ключ</span>
            <textarea value={keys} onChange={(e) => setKeys(e.target.value)}></textarea>
          </label>
        </div>
      )}
      {state.selectedService && (
        <button className="app-srv-block__list__dropdown_item__add-button" type="submit">
          Додати
        </button>
      )}
    </form>
  );
};

export default ServiceForm;
