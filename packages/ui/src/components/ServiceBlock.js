import React, { useEffect, useContext, useRef, useState } from 'react';
import { pdf } from '@react-pdf/renderer';
import { sortingTwits, formatLocalDate } from '../utils/date';
import { StateContext } from '../data/StateProvider';
import { AppContext } from '../App';
import SortingSection from './SortingSection';
import FilterSection from './FilterSection';
import Twit from './Twit';
import ServicePdf from './ServicePdf';

const ServiceBlock = ({ serviceName }) => {
  const { state, dispatch } = useContext(StateContext);
  const { ws, serviceRefs } = useContext(AppContext);
  const [sortOrder, setSortOrder] = useState('desc');
  const anchorRef = useRef(null);
  const sortSectionRef = useRef(null);
  const filterSectionRef = useRef(null);
  const stateTwits = sortingTwits(
    Object.entries(state.services[serviceName]).flatMap(
      ([pageName, page]) => page.isFilterOn && page.twits
    ),
    sortOrder
  );

  const toggleHidden = (el) => {
    el.style.display = el.style.display == 'none' ? 'block' : 'none';
  };
  const generatePdfDocument = async () => {
    const instance = await pdf(<ServicePdf service={serviceName} twits={stateTwits} />).toBlob();
    const url = URL.createObjectURL(instance);
    if (anchorRef.current) {
      anchorRef.current.href = url;
      anchorRef.current.download = `${serviceName}_${formatLocalDate(new Date().getTime())}.pdf`;
    }
  };

  useEffect(() => {
    generatePdfDocument();
  }, [sortOrder]);

  return (
    <section className="app-srv-block__list center-col" ref={serviceRefs[serviceName]}>
      <section className="app-srv-block__list__menu-bar center-row">
        <ul className="app-nav__list top-left">
          <li className="app-nav__list__item">
            <a className="app-nav__list__add-button" ref={anchorRef}>
              🗏
            </a>
          </li>
        </ul>
        <h2>{serviceName}</h2>
        <ul className="app-nav__list top-right">
          <li className="app-nav__list__item">
            <button
              className="app-nav__list__add-button"
              onClick={() => toggleHidden(sortSectionRef.current)}
            >
              ⥮
            </button>
          </li>
          <li className="app-nav__list__item">
            <button
              className="app-nav__list__add-button"
              onClick={() => toggleHidden(filterSectionRef.current)}
            >
              ⧩
            </button>
          </li>
          <li className="app-nav__list__item">
            <button
              className="app-nav__list__add-button"
              onClick={() => dispatch({ type: 'REMOVE_ALL_TWITS', payload: { serviceName } })}
            >
              x
            </button>
          </li>
        </ul>
        <div className="app-srv-block__list__dropdown" ref={sortSectionRef}>
          <SortingSection order={sortOrder} onSort={setSortOrder} />
        </div>
        <div className="app-srv-block__list__dropdown" ref={filterSectionRef}>
          <FilterSection serviceName={serviceName} />
        </div>
      </section>
      <section className="app-srv-block__list__twits">
        {stateTwits.length > 0 &&
          stateTwits.map((twit) => twit && <Twit key={twit.id} twit={twit} />)}
      </section>
    </section>
  );
};

export default ServiceBlock;
