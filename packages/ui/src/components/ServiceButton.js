import React, { useContext } from 'react';
import { StateContext } from '../data/StateProvider';
import { AppContext } from '../App';
import { useImage } from '../utils/useImage';

const ServiceButton = ({ serviceName, isFromAddForm = false, isDissabled = false }) => {
  const { state, dispatch } = useContext(StateContext);
  const { serviceRefs } = useContext(AppContext);
  const { error, image } = useImage(serviceName);

  return (
    <button
      className={
        'app-srv__list__add-button ' + (state.selectedService == serviceName && 'srv--active')
      }
      style={{
        backgroundImage: `url("${image}")`
      }}
      disabled={isDissabled}
      onClick={(e) => {
        !isFromAddForm
          ? serviceRefs[serviceName].current
            ? serviceRefs[serviceName].current.scrollIntoView({
                behavior: 'smooth',
                block: 'center'
              })
            : null
          : (e.preventDefault(), dispatch({ type: 'SELECT_SERVICE', payload: { serviceName } }));
      }}
    ></button>
  );
};

export default ServiceButton;
