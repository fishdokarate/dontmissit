import React, { useContext } from 'react';
import { StateContext } from '../data/StateProvider';
import { formatLocalDate } from '../utils/date.js';

const Twit = ({ twit }) => {
  const { state, dispatch } = useContext(StateContext);

  return (
    <article className="app-srv-block__list__twits__item">
      <div className="app-srv-block__list__twits__menu-bar">
        <span>
          <h3>@{twit.page}</h3>
          <span>
            <b>·</b>
            <a href={twit.link} target="_blank" rel="noreferrer">
              {formatLocalDate(twit.date)}
            </a>
          </span>
        </span>
        <span className="app-srv-block__list__twits__dropdown">
          <button
            className="app-srv-block__list__dropdown_item__add-button"
            onClick={() => {
              dispatch({
                type: 'REMOVE_TWIT',
                payload: {
                  serviceName: twit.service,
                  pageName: twit.page,
                  twitId: twit.id
                }
              });
            }}
          >
            видалити
          </button>
        </span>
      </div>
      <div className="app-srv-block__list__twits__item__text center-col">
        <p>{twit.desc}</p>
        {twit.img && (
          <div
            className="app-srv-block__list__twits__item__img"
            style={{
              backgroundImage: `url(${twit.img})`
            }}
          ></div>
        )}
      </div>
    </article>
  );
};

export default Twit;
