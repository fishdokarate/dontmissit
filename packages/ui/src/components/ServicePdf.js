import React from 'react';
import { Font, StyleSheet, Document, Page, View, Text, Link, Image } from '@react-pdf/renderer';
import { formatLocalDate } from '../utils/date.js';

Font.register({
  family: 'Roboto',
  fonts: [
    {
      src: 'https://cdnjs.cloudflare.com/ajax/libs/ink/3.1.10/fonts/Roboto/roboto-regular-webfont.ttf',
      fontWeight: 400
    },
    {
      src: 'https://cdnjs.cloudflare.com/ajax/libs/ink/3.1.10/fonts/Roboto/roboto-bold-webfont.ttf',
      fontWeight: 600
    }
  ]
});
const styles = StyleSheet.create({
  page: {
    fontFamily: 'Roboto',
    fontSize: '12px',
    padding: '20px'
  },
  page__title: {
    fontSize: '18px',
    fontWeight: 600,
    textAlign: 'center',
    textTransform: 'capitalize',
    marginBottom: '10px'
  },
  view: {
    padding: '20px'
  },
  view__header: {
    fontWeight: 600,
    marginBottom: '10px'
  },
  view__link: {
    fontWeight: 400
  },
  view__text: {
    marginBottom: '20px'
  },
  view__block: {
    display: 'flex',
    flexDirection: 'col',
    alignItems: 'center',
    justifyContent: 'center'
  },
  view__img: {
    height: '180px',
    width: '240px'
  }
});
const ServicePdf = ({ service, twits }) => {
  return (
    <Document>
      <Page size="A4" style={styles.page}>
        <Text style={styles.page__title}>{service}</Text>
        {twits.length > 0 &&
          twits.map((twit) =>
            twit ? (
              <View style={styles.view} key={twit.id}>
                <Text style={styles.view__header}>
                  @{twit.page} ·{' '}
                  <Link style={styles.view__link} src={twit.link}>
                    {formatLocalDate(twit.date)}
                  </Link>
                </Text>
                <Text style={styles.view__text}>{twit.desc}</Text>
                <View style={styles.view__block}>
                  {twit.img ? <Image style={styles.view__img} src={twit.img} /> : null}
                </View>
              </View>
            ) : null
          )}
      </Page>
    </Document>
  );
};

export default ServicePdf;
