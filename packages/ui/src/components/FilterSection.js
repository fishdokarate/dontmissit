import React, { useRef, useContext } from 'react';
import { StateContext } from '../data/StateProvider';

const FilterSection = ({ serviceName }) => {
  const { state, dispatch } = useContext(StateContext);

  return (
    <ul className="app-srv-block__list__dropdown_block">
      {Object.keys(state.services[serviceName]).length > 0 &&
        Object.entries(state.services[serviceName]).map(
          ([pageName, page]) =>
            page && (
              <li className="app-srv-block__list__dropdown_item" key={pageName}>
                <span>{pageName}</span>
                <span>
                  <button
                    className={
                      'app-srv-block__list__dropdown_item__add-button ' +
                      (page.isFilterOn && 'filter--active')
                    }
                    onClick={() =>
                      dispatch({
                        type: 'FILTER_PAGE',
                        payload: { serviceName, pageName, isFilterOn: !page.isFilterOn }
                      })
                    }
                  >
                    фiльтр
                  </button>
                  <button
                    className="app-srv-block__list__dropdown_item__add-button"
                    onClick={() =>
                      dispatch({
                        type: 'REMOVE_PAGE',
                        payload: { serviceName, pageName }
                      })
                    }
                  >
                    видалити
                  </button>
                </span>
              </li>
            )
        )}
    </ul>
  );
};

export default FilterSection;
