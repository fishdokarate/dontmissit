import React, { useEffect, useContext } from 'react';
import { StateContext } from '../data/StateProvider';

const Modal = () => {
  const { state, dispatch } = useContext(StateContext);

  useEffect(() => {
    const timer = setTimeout(() => {
      dispatch({ type: 'CLOSE_MODAL' });
      clearTimeout(timer);
    }, 2000);
  }, [state.isModalOpened]);

  return (
    <div className="app-modal header-container">
      <span>{state.modalContent}</span>
    </div>
  );
};

export default Modal;
