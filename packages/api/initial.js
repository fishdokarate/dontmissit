export const defaultBrowser = {
  product: 'chrome',
  channel: 'chrome',
  args: ['--lang=en-US,en', '--disable-notifications'],
  headless: true,
  setViewport: { width: 1240, height: 680 }
};
  