import { WebSocketServer } from 'ws';

export default (routing, port) => {
  const ws = new WebSocketServer({ port });

  ws.on('connection', (connection, req) => {
    const ip = req.socket.remoteAddress;
    connection.on('message', async (message) => {
      const obj = JSON.parse(message);
      const { type, payload } = obj;
      const handler = routing[type];
      if (!handler) return connection.send('не знайдено', { binary: false });
      const json = JSON.stringify(payload);
      console.log(`${ip} ${type}(${json})`);
      try {
        const result = await handler(payload);
        const data = JSON.stringify(result);
        console.log(`${ip} отримує (${data})`);
        connection.send(data, { binary: false });
      } catch (err) {
        console.dir({ err });
        connection.send('помилка сервера', { binary: false });
      }
    });
  });
  console.log(`api на порті ${port}`);
};
