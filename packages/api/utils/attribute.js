import cheerio from 'cheerio';

const loadContent = (customContent) => {
  const content = customContent || '';
  return cheerio.load(content);
};

const selectBetweenQuote = (selector) => {
  const result = selector.match(/['|"]+(.*)+['|"]|(.*)/);
  return result ? result[1] || result[2] : '';
};

const selectAttribute = (selector) => {
  const result = selector.match(/\[([a-z]*)\]|\[([a-z]*)[\*=|=]+["|'](.*)["|']\]/);
  return { attribute: result ? result[1] || result[2] : '', value: result ? result[3] : '' };
};

const getFromAttribute = (content, selectors) => {
  const $ = loadContent(content);
  for (const selector of selectors.split(',')) {
    if ($(selector).length) {
      const { attribute, value } = selectAttribute(selector);
      switch (attribute) {
        case 'style':
          return selectBetweenQuote($(content).find(selector).css(value));
        // ...
        default:
          return $(content).find(selector).attr(attribute);
      }
    }
  }
};

const isHaveKeywords = (content, keywords) => {
  const result = content.match(new RegExp(keywords.split(',').join('|'), 'gi'));
  return !!result;
};

export { loadContent, getFromAttribute, isHaveKeywords };
