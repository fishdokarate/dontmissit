const getSearchUrl = (serviceUrl, pageName) => {
  return `https://www.${serviceUrl}${pageName ? `/${pageName}/` : ''}`;
};

const formatUrl = (pageUrl) => {
  const result = pageUrl.match(/https:\/\/[^/]+[\/](.*)|\/(.*)/);
  return result ? result[1] || result[2] : '';
};

export { getSearchUrl, formatUrl };
