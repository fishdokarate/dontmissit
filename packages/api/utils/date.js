import moment from 'moment-timezone';

const datetime = moment.tz.setDefault('Etc/UTC');

const sliceDate = (date) => {
  const result = date.match(/(\d+)([а-яa-z]$)/);
  return { num: result ? result[1] : '', token: result ? result[2] : '' };
};

const subtractDate = (num, token) => {
  return datetime().subtract(num, token);
};

const formatDate = (time) => {
  if (!datetime(time).isValid()) {
    const { num, token } = sliceDate(time);
    return +subtractDate(num, token);
  }
  return +datetime(time);
};

const isActualDate = (date, period) => {
  const { num: periodNum, token: periodToken } = sliceDate(period);
  const dateOfNeed = subtractDate(periodNum, periodToken);
  const dateOfPub = formatDate(date);
  return +dateOfNeed <= +dateOfPub;
};

export { formatDate, isActualDate };
