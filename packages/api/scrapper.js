import { randomUUID } from 'crypto';
import puppeteer from 'puppeteer-core';
import moment from 'moment-timezone';
import { loadContent, getFromAttribute, isHaveKeywords } from './utils/attribute.js';
import { getSearchUrl, formatUrl } from './utils/url.js';
import { isActualDate, formatDate } from './utils/date.js';
import { defaultBrowser } from './initial.js';
import credentials from '../../credentials.js';

const workWithBrowser = async (fn) => {
  const browser = await puppeteer.launch(defaultBrowser);
  try {
    return await fn(browser);
  } finally {
    await browser.close();
  }
};

const workWithPage = (browser) => async (fn) => {
  const browserPage = await browser.newPage();
  try {
    return await fn(browserPage);
  } finally {
    await browserPage.close();
  }
};

const goToUrl = async (browserPage, params) => {
  const { url: serviceUrl = '', loginUrl = '', pageName = '' } = params;
  const pageUrl = pageName ? pageName : loginUrl;
  await browserPage.bringToFront();
  await browserPage.goto(getSearchUrl(serviceUrl, pageUrl));
};

const waitForIfExist = async (browserPage, selector) => {
  if (selector) {
    try {
      await browserPage.waitForSelector(selector);
    } finally {
      const $ = loadContent(await browserPage.content());
      return $(selector).length;
    }
  }
  return false;
};

const signIn = async (browserPage, params) => {
  const { serviceName = '', selectors = '' } = params;
  const loginForm = await waitForIfExist(browserPage, selectors.loginForm);
  if (loginForm) {
    (await waitForIfExist(browserPage, selectors.username)) &&
      (await browserPage.type(selectors.username, credentials[serviceName].username));
    (await waitForIfExist(browserPage, selectors.nextButton)) &&
      (await browserPage.click(selectors.nextButton));
    (await waitForIfExist(browserPage, selectors.phoneOrUsername)) &&
      (await browserPage.type(
        selectors.phoneOrUsername,
        credentials[serviceName].username.match(/(.*)@/)[1]
      ),
      await browserPage.click(selectors.phoneOrUsernameButton));
    (await waitForIfExist(browserPage, selectors.password)) &&
      (await browserPage.type(selectors.password, credentials[serviceName].password),
      await browserPage.click(selectors.loginButton));
    await waitForIfExist(browserPage, selectors.main);
  }
};

const signPreview = async (browserPage, params) => {
  const { selectors = '' } = params;
  const previewForm = await waitForIfExist(browserPage, selectors.previewForm);
  if (previewForm) {
    (await waitForIfExist(browserPage, selectors.previewButton)) &&
      (await browserPage.click(selectors.previewButton));
    await waitForIfExist(browserPage, selectors.main);
  }
};

const autoScroll = async (browserPage, params) => {
  const { selectors = '', period = '' } = params;
  let isNotActualDate = true;
  const scrollDelay = selectors.scrollDirection === 'bottom' ? 600 : -600;

  while (isNotActualDate) {
    await waitForIfExist(browserPage, selectors.time);
    await browserPage.mouse.wheel({ deltaY: scrollDelay });
    const $ = loadContent(await browserPage.content());
    const dates = $(selectors.time)
      .map(function () {
        return getFromAttribute(this.parent, selectors.time) || $(this.parent).text();
      })
      .get();
    const lastElem = selectors.scrollDirection === 'bottom' ? dates.length - 1 : 0;
    isNotActualDate = isActualDate(dates[lastElem], period);
  }
};

const receiveTwits = async (browserPage, params) => {
  const { serviceName = '', pageName = '', keywords = '', selectors = '', period = '' } = params;
  await waitForIfExist(browserPage, selectors.twit);
  const $ = loadContent(await browserPage.content());

  const twits = $(selectors.twit)
    .map(function () {
      const time =
        getFromAttribute(this, selectors.time) || $(this).find(selectors.time).text() || '';
      const desc = $(this).find(selectors.desc).text() || '';
      if (time && isActualDate(time, period) && isHaveKeywords(desc, keywords)) {
        const img =
          getFromAttribute(this, selectors.img) || $(this).find(selectors.img).attr('src') || '';
        const url = formatUrl($(this).find(selectors.link).attr('href')) || '';
        return {
          id: `${randomUUID()}_${pageName}`,
          desc,
          img,
          link: getSearchUrl(selectors.url, url),
          date: formatDate(time),
          service: serviceName,
          page: pageName
        };
      }
      return;
    })
    .get();
  return twits;
};

export default async (params) => {
  const { data = {}, selectors = '', period = '' } = params;

  return await workWithBrowser(async (browser) => {
    try {
      const result = await Promise.all(
        data.map(async ({ serviceName, pageName, keywords }) => {
          return await workWithPage(browser)(async (browserPage) => {
            await goToUrl(browserPage, { ...selectors[serviceName] });
            await signIn(browserPage, { serviceName, selectors: selectors[serviceName] });
            await goToUrl(browserPage, { ...selectors[serviceName], pageName });
            await signPreview(browserPage, { selectors: selectors[serviceName] });
            await autoScroll(browserPage, { selectors: selectors[serviceName], period });
            const twits = await receiveTwits(browserPage, {
              serviceName,
              pageName,
              keywords,
              selectors: selectors[serviceName],
              period
            });
            return { serviceName, pageName, twits };
          });
        })
      );
      return { data: result };
    } catch (err) {
      const errText = err + '';
      console.error(errText);
      return { err: errText };
    }
  });
};
