import serverWs from './ws.js';
import scrape from './scrapper.js';

const routing = {
  FETCH_DATA(params) {
    return scrape(params);
  }
};

serverWs(routing, 8000);
