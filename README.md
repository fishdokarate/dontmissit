# Dont miss it (Beta)

Monitoring of important sources. <br />
Current available resources:
<ul>
  <li>Facebook</li>
  <li>Twitter</li>
  <li>Telegram</li>
</ul>

**Feature**

[ ] Show more handler<br />
[ ] Settings: make change update period<br />
[ ] Resolve error stacking<br />
[ ] Sorting twits<br />
[ ] Remove twit<br />
[ ] Searching in service<br />
[ ] Searching by keywords
[ ] Change update period<br />
[ ] Memorize pages<br />
[ ] Memorize twits<br />
[ ] Export to file<br />
[ ] Notification<br /> 
[ ] Settings: on/off notification<br />
[ ] Parallelization in logic<br />
[ ] Tor integration<br />
[ ] Recommended sources button<br />
[ ] Multi languages<br />

**How to start app**

`npm i`<br />
`npm run start`

You can change update period in `./src/initial.js -> defaultPeriod` .

<i>Currently, information is receive only from public resources!</i>
